$(document).ready(function() {
    // validate form in jquery
    function validateEmail(email_check) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email_check);
    }
    $('#loginform').submit(function(){
    	var email = $.trim($('#email').val());
        var flag = true;
        // name_display
        if(email == ''){
        	$('#mail_null').remove();
        }
        if (validateEmail(email)){
            $('#email_error').text('');
        } else{
            if(email == ""){
                $('#email_error').text('Email không được để trống!');
                flag = false;
            }else{
                $('#email_error').text(email+' email của bạn không đúng định dạng!');
                flag = false;
            }  
        }
        return flag;
    });
    
	// $('#click_down').click(function() {
	    // window.location.href = 'http://askwhy.vn/';
	    // return false;
	// });

});