
$( document ).ready(function() {
    $('#pass, #confirm_pass').on('keyup', function () {
        if ($('#pass').val() == $('#confirm_pass').val()) {
            $('#confirmMessage').html('Matching').css('color', 'green');
            $('#submit_form').attr("disabled", false);
        } else{
            $('#confirmMessage').html('Not Matching').css('color', 'red');
            $('#submit_form').attr("disabled", true);
        }
    });
});
