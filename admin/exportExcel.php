<?php
	session_start();
	include ("../mysql.php");
	include ("../function.php");
	if (isset($_SESSION['id']) == null && isset($_SESSION['account']) == null){
		redirect_url('login.php');
	}else{
		//Xuất ra file trùng
		require_once 'PHPExcel/PHPExcel.php';
		if(isset($_SERVER['REQUEST_METHOD']) == "POST"){
			$name_file = "_".date("Y")."_".date("m")."_".date("d")."_".date("H")."_".date("i")."_".date("s");
			
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
	        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(60);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
	        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	        $objPHPExcel->getActiveSheet()->getStyle('1')->getFont()->setBold(true);
	        $objPHPExcel->getActiveSheet()->getStyle('1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
	
			$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('A1:D1','Danh sách email')
			->setCellValue('A3', 'STT')
			->setCellValue('B3', 'Tên')
			->setCellValue('C3', 'Email')
			->setCellValue('D3', 'Link');
			//query
			$query_data = "SELECT * FROM info_user ORDER BY id asc";
			$info_user_data = @mysql_query($query_data);
			$member = @mysql_fetch_array($info_user_data );
			$k = 4;
			$i = 1;
			while($row = mysql_fetch_array($info_user_data)){
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$k, $i)
				->setCellValue('B'.$k, $row[1])
				->setCellValue('C'.$k, $row[2])
				->setCellValue('D'.$k, $row[3]);
				$k++; 
				$i ++;
			}
			
			$objPHPExcel->getActiveSheet()->setTitle('Danh sách trùng');
			$objPHPExcel->setActiveSheetIndex(0);
	
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$full_path = 'file/list'.$name_file.'.xlsx';
			if(!$objWriter->save($full_path)){
				$error = "<p class='alert alert-danger'>Trùng. <a href='".$full_path."'>Download now</a></p>";
			}
			
			echo isset($error) ? $error : '';
			
		}

	}
?>