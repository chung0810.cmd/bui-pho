<?php
	session_start();
	include ("../mysql.php");
	include ("../function.php");
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery.js"></script>
</head>

<body class="bg_login">

<div class="container">
	
<?php
	if(isset($_POST['submit'])){
			$username = addslashes($_POST['account']);
			$password = md5( addslashes( $_POST['password'] ));
			$sql_query = @mysql_query("SELECT id, account, password FROM admin WHERE account='$username'");
			$member = @mysql_fetch_array( $sql_query );
			//var_dump($member); die;
			$check = true;
			if (@mysql_num_rows( $sql_query ) <= 0 ){
				$err_acc = "Tên truy nhập không tồn tại.";
				$check = false;
			}
			if ( $password != $member['password'] ){
				$err_pass = "Nhập sai mật khẩu.";
				$check = false;
			}

			if($check == true){
				$_SESSION['id'] = $member['id'];
				$_SESSION['account'] = $member['account'];
				redirect_url('index.php');
			}
			
	}

	?>
	<div class="loginwrapper_w">
		<h1 class="logintitle">
			<span class="iconfa-lock"></span>sign in
			<span class="subtitle">Hello! Sign in to get you started!</span>
		</h1>
		<div class="loginwrapper">
			<form id="loginform" action="login.php" method="post">
				<p class="animate4 bounceIn">
					<input id="username" type="text" placeholder="Acount" name="account" />
					<span id="username_error" class="error"></span>
				</p>
				<p class="animate4 bounceIn">
					<input id="password" type="password" placeholder="Password" name="password" />
					<span id="password_error" class="error"></span>
				</p>
				<p class="submit">
					<input class="btn btn-default btn-block check_submit" type="submit" name="submit" value="Submit" />
				</p>
			</form>
			<p class="animate7 fadeIn">
				<?php
					if(isset($err_pass)){ echo $err_pass;}
				?>
			</p>
		</div>
	</div>
	
</div>
<script type="text/javascript">
    $(document).ready(function() {
        // validate form in jquery
        $('#loginform').submit(function(){
            var username    = $.trim($('#username').val());
            var password    = $.trim($('#password').val());
            var flag = true;
            // Username
            if (username == ''){
                $('#username_error').text('Acount not null');
                flag = false;
            }
            else{
                if(username.length < 4){
                    $('#username_error').text('Min 4 characters');
                    flag = false;
                }else{
                    $('#username_error').text('');
                }  
            }
            // Password
            if(password == ''){
                $('#password_error').text('Password not null');
                flag = false;
            }else{
                if (password.length < 6){
                    $('#password_error').text('Min 6 characters');
                    flag = false;
                }else{
                    $('#password_error').text('');
                } 
            }
            return flag;
        });

    });
</script>
</body>
</html>
