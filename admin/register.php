<?php
	session_start();
	include ("../mysql.php");
	include ("../function.php");
	if (isset($_SESSION['id']) == null && isset($_SESSION['account']) == null){
		redirect_url('login.php');
	}else{
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Register</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery.js"></script>
</head>

<body class="bg_login">

<div class="container">
<?php
	if (isset($_POST['submit'])){
		$displayname = $_POST['display_name'];
		$username = addslashes( $_POST['account'] );
		$password = md5( addslashes( $_POST['password'] ) );
		$not_user= '';
		if (@mysql_num_rows(@mysql_query("SELECT account FROM admin WHERE account='{$username}'"))>0)
		{
			$not_user = 'Tài khoản: '.$username.' này đã tồn tại, Bạn vui lòng chọn tài khoản khác.';
		}
		if($not_user == ''){
			$sql = "INSERT INTO admin (display_name, account, password) VALUES ('"."$displayname"."','"."$username"."','"."$password')";
			$tv_sql = @mysql_query($sql);
			if ($tv_sql){
				$result = "Thêm mới thành công!";
			}else{
				$result = "Có lỗi trong quá trình đăng kí, xin đăng ký lại!";
			}
		}
		
	}
?>
	<div class="loginwrapper_w">
		<h1 class="logintitle">
			Đăng ký
		</h1>
		<div class="loginwrapper">
			<form id="loginform" action="register.php" method="post">
				<p class="animate4 bounceIn">
					<input id="name_display" type="text" placeholder="Tên hiển thị" name="display_name" />
					<span id="name_display_err" class="error"></span>
				</p>
				<p class="animate4 bounceIn">
					<input id="username" type="text" placeholder="Tài khoản" name="account" />
					<span id="username_error" class="error"></span>
					<span class="error"><?php if(isset($not_user) != ''){ echo $not_user;} ?></span>
				</p>
				<p class="animate4 bounceIn">
					<input id="password" type="password" placeholder="Mật khẩu" name="password" />
					<span id="password_error" class="error"></span>
				</p>
				<p class="submit">
					<input class="btn btn-default btn-block check_submit" type="submit" name="submit" value="Submit" />
				</p>
			</form>
			<p class="animate7 fadeIn error" >
				<?php 
					if(isset($result)){ echo $result;}
				?>
			</p>
		</div>
	</div>

</div>
<script type="text/javascript">
    $(document).ready(function() {
        // validate form in jquery
        $('#loginform').submit(function(){
        	var name_display = $.trim($('#name_display').val());
            var username = $.trim($('#username').val());
            var password= $.trim($('#password').val());
            var flag = true;
            // name_display
            if (name_display == ''){
                $('#name_display_err').text('Tên hiển thị không được trống');
                flag = false;
            }
            else{
                $('#name_display_err').text('');
            }
            // Username
            if (username == ''){
                $('#username_error').text('Tài khoản không được để trống!');
                flag = false;
            }
            else{
                if(username.length < 4){
                    $('#username_error').text('Min 4 characters');
                    flag = false;
                }else{
                    $('#username_error').text('');
                }  
            }
            // Password
            if(password == ''){
                $('#password_error').text('Mật khẩu không được để trống!');
                flag = false;
            }else{
                if (password.length < 6){
                    $('#password_error').text('Min 6 characters');
                    flag = false;
                }else{
                    $('#password_error').text('');
                } 
            }
            return flag;
        });

    });
</script>
</body>
</html>
<?php } ?>
