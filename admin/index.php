<?php 
	session_start();
	include ("../mysql.php");
	include ("../function.php");
	if (isset($_SESSION['id']) == null && isset($_SESSION['account']) == null){
		redirect_url('login.php');
	}else{
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Admin pannel</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery.js"></script>
</head>

<body class="bg_login">
	<div class="container">
		<?php
				$user_id = intval($_SESSION['id']);
				$sql_query = @mysql_query("SELECT * FROM admin WHERE id='{$user_id}'");
				$member = @mysql_fetch_array( $sql_query );
		?>
			<div class="pannel_admin">
				<h1 class="col-lg-12">Xin chào! <?php echo $member['display_name']; ?></h1>
				<a class="btn btn-default btn-block" href='logout.php'>Thoát ra</a>
				<form class="form_exel" action="" method="post" accept-charset="utf-8">
					<input class="btn btn-default btn-block" type="submit" name="export_excel" value="Danh sách email" />
				</form>
			</div>
			<div class="loginwrapper_w">
				<div class="loginwrapper">
					<form action="index.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
						<h3 style="margin: 0 0 15px; color: #fff;">Vui lòng chọn file (xls, xlsx)</h3>
						<p class="animate4 bounceIn">
							<input type="file" name="file" value="" id="file"/>
						</p>
						<p class="submit">
							<input class="btn btn-default btn-block check_submit" type="submit" name="submit" value="Submit" />
						</p>
					</form>
				</div>
			</div>
		<?php
			// Xử lý upload file
			require_once 'PHPExcel/PHPExcel.php';
			if(isset($_POST['submit'])){
				//Xử lý file upload
				if(!empty($_FILES["file"]["name"])){
					//Tách phần mở rộng của file
					$ext = strstr(trim($_FILES["file"]["name"]), '.');
					//Mảng chứa định dạng cho phép
					$allows = array('.xls', '.xlsx');
					if(in_array($ext, $allows)){
						@set_time_limit(3000000);//Time out
						
						$rename = "ask_".date("Y")."_".date("m")."_".date("d")."_".date("H")."_".date("i")."_".date("s").$ext;
						//Đọc file
						$filename = $_FILES["file"]["tmp_name"];
						$inputFileType = PHPExcel_IOFactory::identify($filename);
						$objReader = PHPExcel_IOFactory::createReader($inputFileType);
						 
						$objReader->setReadDataOnly(true);
						/**  Load $inputFileName to a PHPExcel Object  **/
						$objPHPExcel = $objReader->load("$filename");
						 
						$total_sheets=$objPHPExcel->getSheetCount();
						 
						$allSheetName=$objPHPExcel->getSheetNames();
						$objWorksheet  = $objPHPExcel->setActiveSheetIndex(0);
						$highestRow    = $objWorksheet->getHighestRow();
						$highestColumn = $objWorksheet->getHighestColumn();
						$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
						$arraydata = array();
						for ($row = 4; $row <= $highestRow;++$row)
						{
						    for ($col = 0; $col <$highestColumnIndex;++$col)
						    {
						        $value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
						        if(count($value)){
						        	$arraydata[$row-4][$col]=$value;
						        }						        
						    }
						}
						
						$arra_exs = array();//Mảng chứa các dữ liệu trùng
						$success_num = 0;
						$error_num = 0;
						$arr_mail_null = array();
		                $arraydata = array_filter($arraydata);
						
						foreach ($arraydata as $key => $value) {
							$name_user = $value[1];
							if(isset($value[2]) != ''){
								$mail_user = $value[2];
							}else{ $mail_user = '';}
							$code_random = randomcode(10);
							$date_now = time();
							if(isset($value[3]) != ''){
								$phone = $value[3];
							}else{ $phone = '';}
							
							if($mail_user == ''){ // check mail trống
								$name_mail_null = array($name_user);
								$arr_mail_null[] = $name_mail_null;
							}else{
								$check_email = @mysql_query("SELECT email FROM info_user WHERE email='".$mail_user."'");
								if (@mysql_num_rows($check_email) > 0)
								{
									$row_error = @mysql_fetch_array($check_email);
									$exist = array($row_error['email']);
									$arra_exs[] = $exist;
								}else{
									$sql = "INSERT INTO info_user(name, email, userkey, datecreate, phone) VALUES ('".$name_user."','".$mail_user."','".$code_random."','".$date_now."','".$phone."')";
									$tv_sql = @mysql_query($sql);
									if($tv_sql){
										$success_num = $success_num + 1;
									}else{
										$error_num = $error_num + 1;
									}
								}
							}

						}
					}else{
						$error = '<div class="error_inser">File không đúng định dạng!</div>';
					}
				}else{
					$error = '<div class="error_inser">Vui lòng chọn file upload!</div>';
				}
				//Xử lý upload file lên server
				if(!move_uploaded_file($filename, "file/".$rename)){
					$error_move_file = '<div class="center alert alert-warning" role="alert">Chưa upload được file lên hệ thống, nhưng dữ liệu đó bạn vẫn được lưu!</div>';
				}
			}
			echo isset($error) ? $error : '';
			echo isset($error_move_file) ? $error_move_file : '';
			$n_mail_null = 0;
			if(isset($arr_mail_null)){
				$n_mail_null = count($arr_mail_null);
			}
			
			if(isset($arraydata)){
				echo "<div class='color_tex'>Tổng số trường: ".count($arraydata)."</div>";
			}
			if(isset($success_num)){
				echo "<div class='color_tex'>Tổng số trường được thêm mới: ".$success_num."</div>";
			}
			if(isset($error_num)){
				echo "<div class='color_tex'>Tổng số trường thêm mới lỗi: ".$error_num."</div>";
			}
			
			if(isset($arra_exs)){
				$count_mail_exs = count($arra_exs);
				echo "<p class='color_tex'>Tổng số email trùng: ".$count_mail_exs."</p><div class='col-lg-6'>";
				echo "<h3 class='color_tex'>Danh sách các email trùng</h3>";
				$j = 1;
				echo "<table class='style_table'><tr>";
				echo "<td width='30%' class='bor_right'>STT</td><td width='70%'>Tên mail trùng</td></tr>";
				foreach ($arra_exs as $key => $val_mail) {
					echo '<tr><td class="bor_right">'.$j.'</td>';
				    echo '<td>'.$val_mail['0'].'</td></tr>';
					$j ++;
				}
				echo "</table></div>";
			}
			
			if(isset($arr_mail_null)){
				echo "<div class='col-lg-6'>";
				echo "<h3 class='color_tex'>Danh sách các khách hàng không có mail</h3>";
				echo "<div class='color_tex'>Tổng số khách hàng không có mail: ".$n_mail_null."</div>";
				$n = 1;
				echo "<table class='style_table'><tr>";
				echo "<td width='30%' class='bor_right'>STT</td><td width='70%'>Tên khách hàng</td></tr>";
				foreach ($arr_mail_null as $keys => $val_mail_null) {
					echo '<tr><td class="bor_right">'.$n.'</td>';
				    echo '<td>'.$val_mail_null['0'].'</td></tr>';
					$n ++;
				}
				echo "</table></div>";
			}
			
			// export
			if(isset($_POST['export_excel'])){
				$query_data = mysql_query("SELECT * FROM info_user ORDER BY id ASC");
				$datas = array();
				if(mysql_num_rows($query_data) > 0){
					while ($query_datas = mysql_fetch_array($query_data)) {
						$datas[] = $query_datas;
					}
				
					$name_file = "_".date("Y")."_".date("m")."_".date("d")."_".date("H")."_".date("i")."_".date("s");
					$objPHPExcel = new PHPExcel();
					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
			        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
					$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
					$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(60);
					$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
	
					$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
			        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(15);
			        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()->getRowDimension('A1')->setRowHeight(25);
					
					$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('A1:E1')
					->setCellValue('A1', 'Danh sách email')
					->setCellValue('A3', 'STT')
					->setCellValue('B3', 'Tên')
					->setCellValue('C3', 'Email')
					->setCellValue('D3', 'Link')
					->setCellValue('E3', 'Phone');
					//export
					$link_defaul = 'http://'.$_SERVER['HTTP_HOST'].'/tailieu/linkfile.php?key=';
					$k = 4;
					$i = 1;
					foreach ($datas as $row){
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$k, $i)
						->setCellValue('B'.$k, $row[1])
						->setCellValue('C'.$k, $row[2])
						->setCellValue('D'.$k, $link_defaul.$row[3])
						->setCellValue('E'.$k, $row[5]);
						$k++; 
						$i ++;
					}
					$objPHPExcel->getActiveSheet()->setTitle('List link');
					$objPHPExcel->setActiveSheetIndex(0);
			
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
					$full_path = 'file/list'.$name_file.'.xlsx';
					if(!$objWriter->save($full_path)){
						$link_now = "<p class='link_excel'><a href='".$full_path."'>Danh sách file excel</a></p>";
					}
					echo isset($link_now) ? $link_now : '';
				
				}else{
					echo "<p class='alert alert-danger link_excel'>Không có dữ liệu</p>";
				}
			}

		?>
	</div>
</body>
</html>
<?php } ?>
