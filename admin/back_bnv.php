<?php
	// Xử lý upload file
	if($_SERVER['REQUEST_METHOD'] == "POST"){
		if(isset($_POST['partner']) && filter_var($_POST['partner'], FILTER_VALIDATE_INT)){
			$partner = strip_tags(mysql_real_escape_string($_POST['partner']));
			if(isset($_POST['ticket_type_se']) && filter_var($_POST['ticket_type_se'], FILTER_VALIDATE_INT)){
				$ticket_type = (int)$_POST['ticket_type_se'];
				if(isset($_POST['tien_to']) && !empty($_POST['tien_to'])){
					$tien_to = strip_tags(mysql_real_escape_string($_POST['tien_to']));
				}else{
					$tien_to = "BNV";
				}
				//Xử lý file upload
				if(!empty($_FILES["file"]["name"])){
					//Tách phần mở rộng của file
					$ext = end(explode(".", $_FILES["file"]["name"]));
					//Mảng chứa định dạng cho phép
					$allows = array('xls', 'xlsx');
					if(in_array($ext, $allows)){
						@set_time_limit(3000000);//Time out
						//Sửa tên file theo năm tháng ngày giờ phút giây
						$rename = $_SESSION['email']."_".$tien_to."_".date("Y")."_".date("m")."_".date("d")."_".date("H")."_".date("i")."_".date("s").".".$ext;

						//Đọc file
						$filename=$_FILES["file"]["tmp_name"];
						$inputFileType = PHPExcel_IOFactory::identify($filename);
						$objReader = PHPExcel_IOFactory::createReader($inputFileType);
						 
						$objReader->setReadDataOnly(true);
						 
						/**  Load $inputFileName to a PHPExcel Object  **/
						$objPHPExcel = $objReader->load("$filename");
						 
						$total_sheets=$objPHPExcel->getSheetCount();
						 
						$allSheetName=$objPHPExcel->getSheetNames();
						$objWorksheet  = $objPHPExcel->setActiveSheetIndex(0);
						$highestRow    = $objWorksheet->getHighestRow();
						$highestColumn = $objWorksheet->getHighestColumn();
						$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
						$arraydata = array();
						for ($row = 2; $row <= $highestRow;++$row)
						{
						    for ($col = 0; $col <$highestColumnIndex;++$col)
						    {
						        $value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
						        if(count($value)){
						        	$arraydata[$row-2][$col]=$value;
						        }						        
						    }
						}
						$arra_exs = array();//Mảng chứa các dữ liệu trùng
						$arra_erro = array();//mảng chứa mã vé sai
						$arr_no_insert = array(); //Không insert được
						$i = 0;
                        //echo '<pre>';
//                        print_r($arraydata);die;
                        $arraydata = array_filter($arraydata);
                       
						foreach ($arraydata as $key => $value) {				                              						   
							//$ticket_type = 2; //Loại vé
								$phone = strip_tags(mysql_real_escape_string($value[1])); //Số ĐT
                                if (!substr($phone, 0, 1) == 0)
                                	$phone = '0'.$phone;
                                $arrPhone = explode(';',$phone);   
                                $sql = array('0'); // Stop errors when $words is empty
                                $name = $tien_to."_".strip_tags(mysql_real_escape_string($value[0])).'('.$phone.')'; //Tên học viên
                                foreach($arrPhone as $word){
                                    $sql[] = 'partner_phone = \''.$word.'\' or partner_subphone = \''.$word.'\'';
                                }                                        
                                //kiem tra sdt doi tac
                            	/*$check_phone = mysql_query('SELECT id FROM partner WHERE '.implode(" OR ", $sql) . ' limit 1');                                                           
								if(mysql_num_rows($check_phone) > 0){
								   $row = mysql_fetch_array($check_phone); //Partner vừa cập nhật
                                   $last_id = $row['id'];
                                   $phonesub = '';
                                   if($arrPhone[1]){
                                        $phonesub = ", partner_subphone = '$arrPhone[1]'";
                                   }
								   $insert_partner = mysql_query("UPDATE `partner` SET `u_id`= 0,`p_id` = $partner,name='$name', partner_phone = '$arrPhone[0]' $phonesub  WHERE `id` = $last_id");
								}else{*/
								    $insert_partner = mysql_query("INSERT INTO `partner`(`u_id`, `p_id`, `name`, `partner_phone`, `region_id`, `type`, `create_date`, `status`) VALUES (0, '".$partner."', '".$name."', '".$phone."', 1, ".$_POST['coupon'].", NOW(), 1)");
                                    $last_id = last_insert_id(); //Partner vừa thêm vào
								/*}*/
								$arr_ticket = explode(";",$value[2]);

								//Ngay ket thuc
								if(!empty($value[3])){
									$b = str_replace("/", "-", $value[3]);
									$pos = strpos($b, "-");
									if($pos === false){
										$expired_date = "";
									}else{
										$expired_date = $value[3];
									}
								}else{
									$expired_date = "";
								}

								if($insert_partner){
									//Chèn mã vé
									if($_POST['coupon']==1){
									    if(count($arr_ticket) < 2){
										$check_code_ticket = mysql_query("SELECT * FROM `ticket` WHERE `code_ticket` = '".str_replace(" ","",$arr_ticket[0])."'");
										if(mysql_num_rows($check_code_ticket) > 0){
											$row_error = mysql_fetch_array($check_code_ticket);
											$exist = array(
													$arr_ticket[0],
													"Trùng mã vé upload lên ngày ".$row_error['start_date']
													);
												$arra_exs[] = $exist;
										}else{
											$insert_ticket = mysql_query("INSERT INTO `ticket`(`code_ticket`, `type_ticket`, `start_date`, `expired_date`, `partner_id`, `status`) VALUES ('".str_replace(" ","",$arr_ticket[0])."', '".$ticket_type."', NOW(), '".$expired_date."', '".$last_id."', 0)");
											if(!$insert_ticket){
												$e = 1;
											}else{
												$e = 0;
											}
										}
									}else{
										for ($j=1; $j <= count($arr_ticket); $j++) { 
											//Kiểm tra vé trùng
											$check_code_ticket = mysql_query("SELECT * FROM `ticket` WHERE `code_ticket` = '".str_replace(" ","",$arr_ticket[$j-1])."'");
											if(mysql_num_rows($check_code_ticket) > 0){
												$row_error = mysql_fetch_array($check_code_ticket);
												$exist = array(
													$arr_ticket[$j-1],
													"Trùng mã vé upload lên ngày ".$row_error['start_date']
													);
												$arra_exs[] = $exist;
											}else{
												// echo "ok - ".str_replace(" ","",$arr_ticket[$j-1]);
												// echo "<br>";
												$accept = str_replace(" ","",$arr_ticket[$j-1]);
												$insert_ticket = mysql_query("INSERT INTO `ticket`(`code_ticket`, `type_ticket`, `start_date`, `expired_date`, `partner_id`, `status`) VALUES ('".str_replace(" ","",$accept)."', '".$ticket_type."', NOW(), '".$expired_date."', '".$last_id."', 0)");
												if(!$insert_ticket){
													$e = 1;
												}else{
													$e = 0;
												}
											}
										}
									}
								}else{//la coupon                            
								     if(count($arr_ticket) < 2){
								         
										$check_code_ticket = mysql_query("SELECT * FROM `coupon` WHERE `coupon_code` = '".str_replace(" ","",$arr_ticket[0])."'");
	                                    
										if(mysql_num_rows($check_code_ticket) > 0){
											$row_error = mysql_fetch_array($check_code_ticket);
											$exist = array(
													$arr_ticket[0],
													"Trùng mã vé upload lên ngày kết thúc ".$row_error['expired']
													);
												$arra_exs[] = $exist;
											
										}else{											   
											$insert_ticket = mysql_query("INSERT INTO `coupon`(`coupon_code`, `type_ticket`, `id_partner`, `quality`) VALUES ('".str_replace(" ","",$arr_ticket[0])."', '".$ticket_type."', '".$last_id."', 1)");
											if(!$insert_ticket){
												$e = 1;
											}else{
												$e = 0;
											}
										}
									}else{
										for ($j=1; $j <= count($arr_ticket); $j++) { 
											//Kiểm tra vé trùng
											$check_code_ticket = mysql_query("SELECT * FROM `coupon` WHERE `coupon_code` = '".str_replace(" ","",$arr_ticket[$j-1])."'");
											if(mysql_num_rows($check_code_ticket) > 0){
												$row_error = mysql_fetch_array($check_code_ticket);
												$exist = array(
													$arr_ticket[$j-1],
													"Trùng mã vé upload lên ngày ".$row_error['expired']
													);
												$arra_exs[] = $exist;
											}else{
												// echo "ok - ".str_replace(" ","",$arr_ticket[$j-1]);
												// echo "<br>";
												$accept = str_replace(" ","",$arr_ticket[$j-1]);
												$insert_ticket = mysql_query("INSERT INTO `coupon`(`coupon_code`, `type_ticket`, `id_partner`,`quality`) VALUES ('".str_replace(" ","",$accept)."', '".$ticket_type."', '".$last_id."', ".$j.")");
												if(!$insert_ticket){
													$e = 1;
												}else{
													$e = 0;
												}
											}
										}
									}
								}
									
									$i++;
								}else{
									$error = '<div class="center alert alert-warning" role="alert">Lỗi hệ thống!</div>';
								}
							}
                    	
						if(isset($e) && $e == 1){
							$error = '<div class="center alert alert-warning" role="alert">Lỗi hệ thống!</div>';
						}else{
							$success = '<div class="center alert alert-success" role="alert">Thêm thành công!</div>';
						}

						//Xử lý upload file lên server
						if(!move_uploaded_file($filename, "upload/".$rename)){
							$error = '<div class="center alert alert-warning" role="alert">Chưa upload được file lên hệ thống, nhưng dữ liệu đó bạn vẫn được lưu!</div>';
						}

						//Xuất ra file trùng
						if(isset($arra_exs) && !empty($arra_exs)){

							$objPHPExcel = new PHPExcel();
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
				            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
							
							$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
				            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
				            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				            $objPHPExcel->getActiveSheet()->getStyle('1')->getFont()->setBold(true);
				            $objPHPExcel->getActiveSheet()->getStyle('1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);

							$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A1', 'Mã vé hoặc coupon')
							->setCellValue('B1', 'Lý do');
							 
							$k = 2;
							foreach ($arra_exs as $row)
							{
								$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$k, $row[0])
								->setCellValue('B'.$k, $row[1]);
								$k++;
							}
							//die();
							$objPHPExcel->getActiveSheet()->setTitle('Danh sách trùng');
							$objPHPExcel->setActiveSheetIndex(0);

							$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
							$full_path = 'data/danh_sach_trung_bnv_'.$rename;//duong dan file
							if(!$objWriter->save($full_path)){
								//Thông báo ra số lượng dòng trùng
								$exist = count($arra_exs);
								$error = "<p class='alert alert-danger'>Trùng ".$exist."/".count($arraydata)." trường dữ liệu. <a href='".$full_path."'>Download now</a></p>";
							}
						}
					}else{
						$error = '<div class="center alert alert-warning" role="alert">File không đúng định dạng!</div>';
					}
				}else{
					$error = '<div class="center alert alert-warning" role="alert">Vui lòng chọn file upload!</div>';
				}
			}else{
				$error = '<div class="center alert alert-warning" role="alert">Vui lòng chọn loại vé!</div>';
			}
		}else{
			$error = '<div class="center alert alert-warning" role="alert">Vui lòng chọn đối tác cha!</div>';
		}
		//Xử lý file
	}
?>
<article id="article" class="container-fluid">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 none-float" style="margin: 100px auto;">
		<h4 class="center bold" style="margin-bottom: 15px;">Import BNV</h4>
		<form action="" method="POST" enctype="multipart/form-data" accept-charset="utf-8">
			<p class="center">Vui lòng download file mẫu tại <a href="temp/bnv.xlsx">Đây</a> (xls, xlsx)</p>
			<?php
				if(isset($error)){echo $error;} //Hiển thị lỗi nếu có
				if(isset($success)){echo $success;}
			?>
			<div class="center loadding">
				<p style="color: blue;">Đang lưu dữ liệu...</p>
				<p><img src="images/ajax-loader.gif" alt="loadding"></p>
			</div>
            <div class="coupon_import" style="margin: 20px auto;">
				<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
					Vui lòng chọn loại	
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<select name="coupon" required class="form-control" id="Coupon">
						<option value="">Loại</option>
						<option value="1">Vé</option>
                        <option value="2">Coupon</option>
					</select>	
				</div>
				<div class="clear"></div>
			</div> 
			<div class="partnert_import" style="margin: 20px auto;">
				<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
					Vui lòng chọn đối tác 	
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<select name="partner" required class="form-control" id="partner">
						<option value="">Vui lòng chọn đối tác cha</option>
						<?php
                        $sql = "SELECT id,name,partner_phone FROM partner ORDER BY p_id ASC";
        			    $rs = mysql_query($sql);
        			    while($r = mysql_fetch_object($rs)){
        			    	if(!empty($r->partner_phone)){
        			    		$phone = "(".$r->partner_phone.")";
        			    	}else{
        			    		$phone = "";
        			    	}
        			    	echo '<option '.(isset($_GET['p']) && $_GET['p'] == $r->id ? 'selected' : '' ).' value="'.$r->id.'">'.$r->name.$phone.'</option>';
        			    }						
						?>
					</select>	
				</div>
				<div class="clear"></div>
			</div>  			
			
			<div class="partnert_import" style="margin: 20px auto;">
				<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
					Vui lòng chọn loại vé 	
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<select name="ticket_type_se" required class="form-control">
						<option value="">Vui lòng chọn loại vé</option>
						<?php
							$t_ticket = findAll('*', "`ticket_type`", "status = 1");
							if(!empty($t_ticket)){
								foreach ($t_ticket as $key => $value) {
									if(isset($_POST['ticket_type_se']) && $_POST['ticket_type_se'] == $value['id']){
										$s = "selected = 'selected'";
									}else{
										$s = "";
									}
									echo "<option {$s} value='".$value['id']."'>".$value['ticket_type']."</option>";
								}
							}
						?>
					</select>	
				</div>
				<div class="clear"></div>
			</div>
			
			<div class="partnert_import" style="margin: 20px auto;">
				<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
					Tiền tố (Mặc định là BNV) 	
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="tien_to" class="form-control" value="" placeholder="Tiền tố viết liền không dấu">
				</div>
				<div class="clear"></div>
			</div>

			<div class="partnert_import" style="margin: 20px auto;">
				<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
					Vui lòng chọn file (xls, xlsx) 	
				</div> 
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<input type="file" required name="file" value="" id="file" placeholder=""/>
				</div>
				<div class="clear"></div>
			</div>
 
			<div class="col-xs-12 col-md-12 center">					                       
				<input type="submit" name="import" class="btn btn-success" value="Import"/>                               
			</div>  
		</form>
		<div class="clear"></div>
	</div>
</article>
<?php
	include_once('footer.php');
?>
<script>
	$(document).on('click', '.btn-success', function(event) {
		//event.preventDefault();
		/* Act on the event */
		//var partner = $('#partnert').val();
		//var file = $('#file').val();
		$('.loadding').css('display', 'block');
		// if(partner.length > 0 && file.length > 0){
		// 	$('.loadding').css('display', 'block');
		// }

		
	});
</script>
<script type="text/javascript" src="<?= 'http://' . $_SERVER['HTTP_HOST'] ?>/dangky/admincp/js/common.js"></script>
<script type="text/javascript" src="<?= 'http://' . $_SERVER['HTTP_HOST'] ?>/dangky/admincp/js/jquery_ui/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= 'http://' . $_SERVER['HTTP_HOST'] ?>/dangky/js/jquery_ui/jquery-ui.min.css" />
<script type="text/javascript" src="<?= 'http://' . $_SERVER['HTTP_HOST'] ?>/dangky/admincp/js/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" type="text/css" href="<?= 'http://' . $_SERVER['HTTP_HOST'] ?>/dangky/admincp/css/jquery-ui-timepicker-addon.css" />
           
<link rel="stylesheet" href="<?= 'http://' . $_SERVER['HTTP_HOST'] ?>/dangky/admincp/css/prettify.css" type="text/css">
    
<script type="text/javascript" src="<?= 'http://' . $_SERVER['HTTP_HOST'] ?>/dangky/admincp/js/prettify.js"></script>

<link rel="stylesheet" href="<?= 'http://' . $_SERVER['HTTP_HOST'] ?>/dangky/admincp/css/bootstrap-multiselect.css" type="text/css">
<script type="text/javascript" src="<?= 'http://' . $_SERVER['HTTP_HOST'] ?>/dangky/admincp/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        window.prettyPrint() && prettyPrint();
    });
</script>
</head>
<script type="text/javascript">
$(document).ready(function() {
$('#partner').multiselect({
    includeSelectAllOption: true,
    enableFiltering: true,
    maxHeight:388,
    enableCaseInsensitiveFiltering: true,
});
});
</script>