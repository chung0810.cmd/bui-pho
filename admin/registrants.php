
<style type="text/css" media="screen">
	.checkbox{
		margin-top: 0px !important;
	}
</style>
<?php
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		if(!isset($_POST['event']) OR count($_POST['event']) < 1){
			$error = "<p class='alert alert-danger'>Vui lòng chọn ít nhất một khóa học</p>";
		}else{
			$event = $_POST['event'];
			$list_event = implode(",",$event);
			//get list student
			$students = mysql_query("SELECT * FROM `registrants` WHERE `std_event` IN (".$list_event.") AND `deleted` = 0");
			$datas = array();
			if(mysql_num_rows($students) > 0){
				while ($student = mysql_fetch_array($students)) {
					$datas[] = $student;
				}
				$name_file = "danh_sach_hoc_vien_khóa_".$list_event."_".date("Y")."_".date("m")."_".date("d")."_".date("H")."_".date("i")."_".date("s");
				//$rename = $_SESSION['email']."_ticket_".date("Y")."_".date("m")."_".date("d")."_".date("H")."_".date("i")."_".date("s").".".$ext;

				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
	            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
	            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
				
				$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
	            $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	            $objPHPExcel->getActiveSheet()->getStyle('1')->getFont()->setBold(true);
	            $objPHPExcel->getActiveSheet()->getStyle('1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);

				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'Họ tên')
				->setCellValue('B1', 'Email')
				->setCellValue('C1', 'SĐT')
				->setCellValue('D1', 'Ngày tháng đăng ký')
				->setCellValue('E1', 'Khóa học');
				 
				$i = 2;
				foreach ($datas as $data)
				{
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $data['std_name'])
					->setCellValue('B'.$i, $data['std_email'])
					->setCellValue('C'.$i, "0".$data['std_phone'])
					->setCellValue('D'.$i, $data['std_time'])
					->setCellValue('E'.$i, $data['std_event']);
					$i++;
				}
				//die();
				$objPHPExcel->getActiveSheet()->setTitle('Danh sách học viên');
				$objPHPExcel->setActiveSheetIndex(0);

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$full_path = 'data/'.$name_file.'.xlsx';//duong dan file
				if(!$objWriter->save($full_path)){
					//Thông báo ra số lượng dòng trùng
					$error = "<p class='alert alert-success'>Xuất thành công, <a href='".$full_path."'>Download now</a></p>";
				}

			}else{
				$error = "<p class='alert alert-danger'>Không có dữ liệu</p>";
			}
		}
	}
?>
<article id="article" class="container-fluid">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 none-float" style="margin: 100px auto;">
		<h4 class="center bold" style="margin-bottom: 15px;">Xuất dữ liệu (registrants)</h4>
		<form action="" method="POST" enctype="multipart/form-data" accept-charset="utf-8">
			<?php
				if(isset($error)){echo $error;} //Hiển thị lỗi nếu có
				if(isset($success)){echo $success;}
			?>
			<div class="center loadding">
				<p style="color: blue;">Đang xuất dữ liệu...</p>
				<p><img src="images/ajax-loader.gif" alt="loadding"></p>
			</div>

			<div class="partnert_import" style="margin: 20px auto;">
				<div class="col-lg-4 6 col-xs-12">
					Vui lòng chọn khóa học 	
				</div>
				<div class="col-lg-8 col-xs-12">
					<?php
						$khoa_hoc = mysql_query("SELECT * FROM `event`");
						if(mysql_num_rows($khoa_hoc) > 0){
							while ($row = mysql_fetch_array($khoa_hoc)) {
								?>
									<div class="checkbox col-lg-6 col-xs-12">
										<label><input type="checkbox" name="event[]" <?php if(isset($_POST['event']) && in_array( $row['id'], $_POST['event'])){echo "checked = 'checked'";} ?> value="<?php echo $row['id']; ?>"><?php echo $row['event_name']; ?></label>
									</div>
								<?php
							}
						}
					?>	
				</div>
				<div class="clear"></div>
			</div>  			
			<div class="col-xs-12 col-md-12 center">					                       
				<input type="submit" name="export" class="btn btn-success" value="Xuất học viên"/>                               
			</div>  
		</form>
		<div class="clear"></div>
	</div>
</article>
<?php
	include_once('footer.php');
?>
<script>
	$(document).on('click', '.btn-success', function(event) {
		//event.preventDefault();
		$('.loadding').css('display', 'block');
	});
</script>